#!/bin/bash

npm i -D postcss postcss-cli postcss-loader postcss-scss \
  precss postcss-mixins postcss-inline-svg postcss-color-function \
  autoprefixer postcss-import postcss-strip-units \
  postcss-automath postcss-hexrgba postcss-extend postcss-each \
  postcss-fontpath postcss-css-variables
