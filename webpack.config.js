const path = require('path')
const webpack = require('webpack')
const StyleLintPlugin = require('stylelint-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: {
    main: ['./src/css/main.css', './src/js/main']
  },
  output: {
    path: path.join(__dirname, 'dist/assets'),
    filename: '[name].min.js',
    chunkFilename: `[name]-[id].js?version=${Date.now()}`,
    publicPath: `/assets/`
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader'
      },
      {
        test: /\.js$/,
        use: [
          'babel-loader'
        ]
      },
      {
        test: /\.s?css$/,
        use: [
          'style-loader',
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development',
            },
          },
          'css-loader?importLoaders=1&minimize=1',
          'postcss-loader'
        ]
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader?name=/fonts/[name].[ext]'
      }
    ]
  },
  resolve: {
    alias: {
      'lib': path.resolve(__dirname, 'src/js/lib'),
      'mixins': path.resolve(__dirname, 'src/js/mixins'),
      'modules': path.resolve(__dirname, 'src/modules'),
      'vue': process.env.ENV === 'production' ? 'vue/dist/vue.min.js' : 'vue/dist/vue.js',
      'root': path.resolve(__dirname, 'src')
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new StyleLintPlugin({
        files: [
          'src/assets/css/*.css',
          'src/modules/**/*.css'
        ]
    }),
    new MiniCssExtractPlugin({
      filename: '[name].min.css',
      //chunkFilename: '[id].css',
      ignoreOrder: false, // Enable to remove warnings about conflicting order
    })
  ]
}
