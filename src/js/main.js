import init from 'lib/init'
import hmr from 'webpack-hot-middleware/client?reload=true'

hmr.subscribe(function (payload) {
  if (payload.action === 'reload' || payload.reload === true) {
    window.location.reload()
  }
})

document.addEventListener('DOMContentLoaded', () => {
  init({
    module: 'modules'
  }).mount()
})
