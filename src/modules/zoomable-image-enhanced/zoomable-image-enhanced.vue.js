import Vue from 'vue'

Vue.component('zoomable-image-enhanced', resolve => {
  require(['vue-zoom'], vZoom => resolve({
    props: {
      src: String,
      zoom: String,
      enabled: Boolean
    },
    components: {
      vZoom
    },
    template: `
    <div>
      <vue-zoom :img="src" width="100%" :zoom="zoom" />
    </div>
    `
  }))
})
