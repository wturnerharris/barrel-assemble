import cart from 'lib/cart'

export const actions = {
  getCart ({ commit, state }, payload) {
    commit('setLoading', true)

    return cart
      .get()
      .then(data => {
        commit('refreshCart', data)
      })
  },

  updateItemQuantity ({commit, state}, { quantity, id }) {
    if (!id) {
      return
    }

    commit('setLoading', true)

    return cart
      .update({ quantity, id })
      .then(data => {
        commit('refreshCart', data)
      })
  },

  addToCart ({dispatch, state}, { id, quantity }) {
    const { items } = state.app.cart

    // Adds the requested `quantity` to the current item.quantity
    const item = items.find(i => i.variant_id === id)
    const totalQuantity = item
      ? item.quantity + quantity
      : quantity

    return dispatch('updateItemQuantity', { id, quantity: totalQuantity })
  },

  trackAddToCart ({dispatch, state}, { product, id }) {
    const variant = product.variants.find(({id: _id}) => _id === id)

    fbq && fbq('track', 'AddToCart', {
      value: (Number(variant.price) / 100),
      currency: 'USD',
      content_name: variant.name,
      content_type: 'product',
      content_ids: [id]
    })
  }
}
