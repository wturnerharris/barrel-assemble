import { formatPrice } from 'lib/util'

export const getters = {
  subtotal (state) {
    return formatPrice(state.app.cart.total_price)
  }
}
