import Vue from 'vue'

export default el => {
  return new Vue({
    el,
    data () {
      return {
        product: window.BARREL.product,
        activeAccordion: false
      }
    },
    methods: {
      toggleAccordion (reference) {
        if (this.activeAccordion === reference) {
          this.activeAccordion = false
        } else {
          this.activeAccordion = reference
        }
      },
      height (reference) {
        if (this.activeAccordion !== reference) {
          return 0
        }

        return this.$refs[reference].getBoundingClientRect().height + 'px'
      }
    }
  })
}
