import Vue from 'vue'

Vue.component('quantity-selector', {
  props: {
    initial: Number,
    min: {
      type: Number,
      default: 1
    },
    max: {
      type: Number,
      default: 99
    }
  },
  data: function () {
    return {
      value: this.initial
    }
  },
  methods: {
    incrementDown: function (e) {
      this.value = this.value -= 1
    },
    incrementUp: function (e) {
      this.value = this.value += 1
    }
  },
  watch: {
    value: function () {
      if (this.value < this.min) {
        this.value = this.min
        return
      }

      if (this.value > this.max) {
        this.value = this.max
        return
      }

      this.$emit('change', this.value)
    }
  },
  computed: {},
  template: `
  <div class="quantity-selector">
    <button
      class="quantity-selector__action"
      type="button"
      v-on:click="incrementDown"
      :disabled="value <= min">
      <span>-</span>
    </button>
    <input class="quantity-selector__field" type="number" v-model="value" maxlength="2">
    <button
      class="quantity-selector__action"
      type="button"
      v-on:click="incrementUp"
      :disabled="value >= max">
      <span>+</span>
    </button>
  </div>
  `
})
