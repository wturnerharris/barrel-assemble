import Vue from 'vue'
import state from 'lib/appState'
import {handleize} from 'lib/util'
// import vZoom from 'vue-zoom'

export default el => new Vue({
  el,
  components: {
    // vZoom
  },
  data () {
    return {
      key: 'color',
      activeIndex: 0,
      state,
      product: window.BARREL.product
    }
  },
  watch: {
    'state.activeVariant' () {
      this.activeIndex = 0
    }
  },
  computed: {
    imageHandle () {
      for (let i = 0; i < this.product.options.length; i++) {
        const {options = []} = this.product
        const variant = this.state.activeVariant

        if (handleize(options[i]) === this.key) {
          const value = variant[`option${i + 1}`]

          if (!value) {
            return false
          }
          return `${this.key}-${handleize(value)}`.toLowerCase()
        }
      }
      return false
    },
    images () {
      const {images = []} = this.product
      const filtered = images.filter(({src = ''}) => ~src.indexOf(this.imageHandle))
      return filtered.length ? filtered : []
    },
    featuredImage () {
      return (this.images[this.activeIndex] || {}).src || ''
    }
  }
})
