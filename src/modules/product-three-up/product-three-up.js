import Vue from 'vue'
import on from 'dom-event'
import Flickity from 'flickity'
import { isOver } from 'lib/util'

import 'modules/product-card/product-card.vue'
import 'modules/image/image.vue'

const breakpoint = 1000

export default el => new Vue({
  el,
  methods: {
    init () {
      this.$nextTick(() => {
        this.$flickity = new Flickity(this.$refs['carousel'], {
          cellAlign: 'center',
          initialIndex: 1,
          prevNextButtons: false,
          pageDots: false
        })
      })
    },
    destroy () {
      this.$flickity.destroy()
      this.$flickity = null
    },
    onResize () {
      if (!this.$flickity) {
        if (!isOver(breakpoint)) this.init()
      } else {
        if (isOver(breakpoint)) this.destroy()
      }
    }
  },
  mounted () {
    if (!isOver(breakpoint)) this.init()

    on(window, 'resize', () => {
      this.onResize()
    })
  }
})
