import Vue from 'vue'
import store from './@store'
import {mapState, mapActions} from 'vuex'

export default el => new Vue({
  el,
  store: store(),
  data () {
    return {}
  },
  mounted () {
    this.fetchCollection({
      handle: location.pathname,
      ajaxing: false,
      initial: true
    })
  },
  computed: {
    ...mapState([
      'collection',
      'products',
      'mounted',
      'ajaxing',
      'loading',
      'selectedFacets',
      'sort',
      'compactView',
      'isMobile'
    ]),
    ...mapState({
      visibleProducts (state) {
        return (state.products || []).filter(p => this.shouldShowProduct(p))
      }
    })
  },
  methods: {
    shouldShowProduct (product) {
      const productFacetMatches = this.selectedFacets.reduce((obj, facet) => {
        if (typeof obj.needed === 'undefined') {
          obj.needed = {}
        }
        if (typeof obj.matches === 'undefined') {
          obj.matches = {}
        }
        obj.needed[facet.name] = true
        if (
          typeof product[facet.name] !== 'undefined' &&
          ~product[facet.name].indexOf(facet.value)
        ) {
          obj.matches[facet.name] = true
        }
        return obj
      }, {})

      if (!productFacetMatches.needed) {
        return true
      }

      return Object.keys(productFacetMatches.needed).every(key => {
        return typeof productFacetMatches.matches[key] !== 'undefined'
      })
    },
    ...mapActions([
      'fetchCollection',
      'toggleFilterMenu',
      'toggleCompactView'
    ])
  }
})
