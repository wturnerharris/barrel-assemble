module.exports = function (liquid) {
  liquid.registerTag('form', {
    parse: function (tagToken, remainTokens) {
      this.templates = []
      var p

      var stream = liquid.parser.parseStream(remainTokens)
        .on('start', x => {
          p = this.templates
        })
        .on('tag:endform', token => stream.stop())
        .on('template', tpl => p.push(tpl))
        .on('end', x => {
          throw new Error(`tag ${tagToken.raw} not closed`)
        })

      stream.start()
    },

    render: function (scope, hash) {
      return liquid.renderer.renderTemplates(this.templates, scope)
    }
  })
}
