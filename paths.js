const path = require('path')
const fs = require('fs')

const resolver = function(app) {
  // all paths relative to this file!!
  const moduleDir = './src/modules/'
  const rootPaths = [
    path.resolve(__dirname, './src/layout'),
    path.resolve(__dirname, './src/templates')
  ]

  fs.readdir(moduleDir, (err, files) => {
    files.forEach(fileName => {
      // if module-name.json exists
      let moduleData = `${moduleDir}${fileName}/${fileName}.json`
      if (fs.existsSync(moduleData)) {
        app.data(require(path.resolve(__dirname, moduleData)), true)
      }

      // if data.js exists
      let dataFile = `${moduleDir}${fileName}/data.js`
      if (fs.existsSync(dataFile)) {
        app.data(require(path.resolve(__dirname, dataFile)))
      }
      rootPaths.push(path.resolve(__dirname, `${moduleDir}${fileName}`))
    });
  });

  return rootPaths
}

module.exports = resolver;
