'use strict';

const assemble = require('assemble')
const extname = require('gulp-extname');
const { Liquid } = require('liquidjs');
const app = assemble()
const engine = new Liquid({
  root: require('./paths.js')(app),
  extname: '.liquid'
});
require('./liquid/tags')(engine)
require('./liquid/filters')(engine)

app.engine('liquid', function(str, locals, cb) {
  engine.parseAndRender(str, locals).then(view => cb(null, view))
});
app.option('engine', 'liquid');

let config = {
  pages: 'src/pages/**/*.liquid',
  collections: 'src/collections/*.hbs',
  products: 'src/products/*.hbs',
  partials: 'src/partials/**/*.hbs',
  data: [ 'src/data/*.js' ],
  assets: 'dist/assets'
}

app.partials(config.partials);

// load global data
let globals = require('./src/data/global.js')
for (let item in globals) {
  app.data(item, globals[item])
}

// load exported js files
app.dataLoader('js', function(str, fp) {
  if (~fp.indexOf('global')) return null
  let dataFile = require(fp)
  return dataFile;
});
app.data(config.data);
app.pages(config.pages);

app.task('default', function(e) {
  return app.toStream('pages')
    .pipe(app.renderFile())
    .pipe(extname('.htm'))
    .pipe(app.dest('dist'));
});

app.task('assets', function() {
  // return, to let assemble know when the task has completed
  return app.copy('src/assets/{fonts,img}/*', config.assets);
});

module.exports = app;
