# Assemble Static Site Generator

## Table of Contents
1. [Getting Started](#getting-started)
2. [Developing](#developing)
2. [Build Scripts](#using-build-scripts)

## Getting Started

### Dependencies

#### Install Node
https://nodejs.org/en/download/

#### Install Lando
https://github.com/lando/lando/releases/tag/v3.0.0-beta.47

### Intial Setup
Run this command from your project root directory to download the project's dependencies:

```
npm i
```

### Start Lando Server

```
lando start
```

#### Build Project

```bash
npm run build
```

## Developing
After the steps above, you are ready to go. The main command needed to generate the `dist/` is `npm run build`. For more specific tasks see [Build Scripts](#using-build-scripts) for more information.

## Using Build Scripts
There are a variety of build scripts provided in the `package.json` that can be run directly in your terminal window. All scripts can be run via `npm run <scriptName>`.

#### Update Environment
Make sure your node environment is updated. Assuming you're using Homebrew:

```bash
# check kegs and brew version
brew update

# update node
brew upgrade node
```
